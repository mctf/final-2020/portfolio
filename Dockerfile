FROM adoptopenjdk/openjdk14:alpine
WORKDIR /tmp/build

COPY gradle ./gradle
COPY *.gradle ./
COPY gradle.properties ./
COPY gradlew ./
COPY src ./src
COPY resources ./resources
RUN ./gradlew --no-daemon build

FROM adoptopenjdk/openjdk14:debianslim-jre
COPY docker/wait-for-it.sh /
COPY --from=0 /tmp/build/build/libs/portfolio.jar /app.jar
ENTRYPOINT ["./wait-for-it.sh", "pdns:3334", "--timeout=10", "--", "java", "-jar", "/app.jar"]