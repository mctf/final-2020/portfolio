package ru.mctf.service

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.DeserializationFeature
import io.ktor.client.*
import io.ktor.client.engine.apache.*
import io.ktor.client.features.json.*
import io.ktor.client.request.*
import io.ktor.http.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.util.concurrent.TimeUnit

class DnsService {

    companion object {
        private val ttl = TimeUnit.MINUTES.toSeconds(1).toInt()
        private val externalIp = System.getenv("EXTERNAL_IP").ifNullOrEmpty("127.0.0.1")
        private val apiKey = System.getenv("PDNS_API_KEY")
        private const val apiUrl = "http://pdns:3334/api/v1"

        private val httpClient = HttpClient(Apache) {
            install(JsonFeature) {
                serializer = JacksonSerializer() {
                    configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                    setSerializationInclusion(JsonInclude.Include.NON_NULL)
                }
            }
        }

        init {
            runBlocking {
                val zones = httpClient.get<List<Zone>> {
                    url("$apiUrl/servers/localhost/zones?zone=portfolio.ctrl.")
                    header("X-API-Key", apiKey)
                }
                if (zones.isEmpty()) {
                    httpClient.post<Unit> {
                        url("$apiUrl/servers/localhost/zones")
                        contentType(ContentType.Application.Json)
                        body = Zone(
                            name = "portfolio.ctrl."
                        )
                        header("X-API-Key", apiKey)
                    }
                }
            }
        }
    }

    fun createDomain(name: String) {
        runBlocking {
            httpClient.patch<Unit> {
                url("$apiUrl/servers/localhost/zones/portfolio.ctrl.")
                contentType(ContentType.Application.Json)
                body = RRSets(
                    listOf(
                        RRSet(
                            name = "$name.portfolio.ctrl.",
                            ttl = ttl,
                            records = listOf(
                                Record(
                                    content = externalIp
                                )
                            )
                        )
                    )
                )
                header("X-API-Key", apiKey)
            }
        }
    }

    fun deleteDomain(name: String) {
        GlobalScope.launch {
            httpClient.patch<Unit> {
                url("$apiUrl/servers/localhost/zones/portfolio.ctrl.")
                contentType(ContentType.Application.Json)
                body = RRSets(
                    listOf(
                        RRSet(
                            name = "$name.portfolio.ctrl.",
                            changetype = "DELETE"
                        )
                    )
                )
                header("X-API-Key", apiKey)
            }
        }
    }

}

private fun String?.ifNullOrEmpty(altValue: String): String {
    return if (this == null || this.isEmpty()) altValue else this
}

private data class Zone(
    val name: String,
    val kind: String = "Master",
    val dnssec: Boolean = false,
    @JsonProperty("soa-edit") val soaEdit: String = "INCEPTION-INCREMENT",
    val masters: List<String> = emptyList(),
    val nameservers: List<String> = emptyList()
)

private data class RRSets(
    val rrsets: List<RRSet>
)

private data class RRSet(
    val name: String,
    val type: String = "A",
    val ttl: Int? = null,
    val changetype: String = "REPLACE",
    val records: List<Record> = emptyList()
)

private data class Record(
    val content: String,
    val disabled: Boolean = false
)