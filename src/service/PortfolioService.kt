package ru.mctf.service

import ru.mctf.dao.User
import java.io.File
import java.util.*

class PortfolioService {

    private val dir = File("portfolios")
    private val initialContent = javaClass.getResource("/templates/initial_portfolio.html").readText()

    init {
        dir.mkdirs()
    }

    fun getPortfolio(user: User) =
        File(dir, user.id.toString()).readText()

    fun createPortfolio(id: UUID) =
        updatePortfolio(id, initialContent)

    fun updatePortfolio(id: UUID, content: String) =
        File(dir, id.toString()).writeText(content)

    fun deletePortfolio(id: UUID) {
        val portfolio = File(dir, id.toString())
        if (portfolio.exists()) {
            portfolio.delete()
        }
    }
}