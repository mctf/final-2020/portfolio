package ru.mctf.dao

import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.`java-time`.datetime
import java.time.LocalDateTime
import java.util.*

object Users : Table() {
    val id = uuid("id")
    val login = varchar("name", 40).index(isUnique = true)
    val password = varchar("password", 255)
    val domain = varchar("domain", 36).index(isUnique = true)
    val registrationDate = datetime("registration_date")
    override val primaryKey = PrimaryKey(id)
}

data class User(val id: UUID, val login: String, val password: String, val domain: String, val registrationDate: LocalDateTime) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val that = other as User
        return id == that.id
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }
}