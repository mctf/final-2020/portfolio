package ru.mctf.route

import io.ktor.application.*
import io.ktor.locations.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.sessions.*
import ru.mctf.AuthenticatedUser

@KtorExperimentalLocationsAPI
@Location(Paths.LOGOUT_PATH)
class Logout

@KtorExperimentalLocationsAPI
fun Route.logout() {
    get<Logout> {
        call.sessions.clear<AuthenticatedUser>()
        call.respondRedirect(Paths.INDEX_PATH)
    }
}