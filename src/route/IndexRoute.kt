package ru.mctf.route

import io.ktor.application.*
import io.ktor.locations.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.thymeleaf.*
import ru.mctf.service.UserService
import ru.mctf.service.tryAuthenticate

@KtorExperimentalLocationsAPI
@Location(Paths.INDEX_PATH)
class Index

@KtorExperimentalLocationsAPI
fun Route.index(userService: UserService) {
    get<Index> {
        tryAuthenticate(userService) { user ->
            if (user != null) {
                call.respond(ThymeleafContent("index", mapOf("user" to user)))
            } else {
                call.respond(ThymeleafContent("index", emptyMap()))
            }
        }
    }
}