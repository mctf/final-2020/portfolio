package ru.mctf.route

import io.ktor.application.*
import io.ktor.http.*
import io.ktor.locations.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.thymeleaf.*
import ru.mctf.service.PortfolioService
import ru.mctf.service.UserService
import ru.mctf.service.authenticateOrFail

@KtorExperimentalLocationsAPI
@Location(Paths.PORTFOLIO_EDIT_PATH)
class PortfolioEdit

@KtorExperimentalLocationsAPI
fun Route.portfolioEdit(userService: UserService, portfolioService: PortfolioService) {
    get<PortfolioEdit> {
        authenticateOrFail(userService) { user ->
            val content = portfolioService.getPortfolio(user)
            val model = mapOf("user" to user, "domain" to user.domain, "content" to content)

            call.respond(ThymeleafContent("portfolio_edit", model))
        }
    }

    post<PortfolioEdit> {
        authenticateOrFail(userService) { user ->
            val post = call.receive<Parameters>()

            val newContent = post["content"]

            if (newContent == null || newContent.length > 512) {
                call.respondText("Incorrect content", status = HttpStatusCode.BadRequest)
                return@post
            }

            portfolioService.updatePortfolio(user.id, newContent)
            call.respondRedirect(Paths.INDEX_PATH)
        }
    }
}