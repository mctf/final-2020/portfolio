package ru.mctf.route

class Paths {
    companion object {
        const val INDEX_PATH = "/"
        const val REGISTER_PATH = "/register"
        const val LOGIN_PATH = "/login"
        const val LOGOUT_PATH = "/logout"
        const val LATEST_PATH = "/latest"
        const val PORTFOLIO_EDIT_PATH = "/edit"
    }
}