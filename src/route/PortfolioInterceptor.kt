package ru.mctf.route

import io.ktor.application.*
import io.ktor.http.*
import io.ktor.locations.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.thymeleaf.*
import ru.mctf.service.DomainService

@KtorExperimentalLocationsAPI
fun Route.portfolioInterceptor(domainService: DomainService) {
    intercept(ApplicationCallPipeline.Call) {
        val host = call.request.headers["Host"] ?: return@intercept
        val match = domainService.urlPattern.matchEntire(host)
        if (match != null) {
            val domainName = match.groupValues[1]
            val owner = domainService.getDomainUser(domainName)
            if (owner != null) {
                val contentFileName = owner.id.toString()
                call.respond(ThymeleafContent("portfolio", mapOf("user" to owner.login, "contentFile" to contentFileName)))
                finish()
            } else {
                call.respond(status = HttpStatusCode.NotFound, message = "Not found")
                finish()
            }
        }
    }
}