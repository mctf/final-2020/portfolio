from portfolio import pull_flag
import sys
from redis import StrictRedis

redisHost = sys.argv[1]
redisPassword = sys.argv[2]
ip = sys.argv[3]
oldFlag = sys.argv[4]

redis = StrictRedis(host=redisHost, port=6379, password=redisPassword, decode_responses=True)

domain = redis.get(f'checkers_state/portfolio/{ip}/domain')
pull_flag(ip, domain, oldFlag)
