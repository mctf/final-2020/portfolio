from portfolio import push_flag
import sys
from redis import StrictRedis

redisHost = sys.argv[1]
redisPassword = sys.argv[2]
ip = sys.argv[3]
newFlag = sys.argv[4]

domain = push_flag(ip, newFlag)

if domain:
    redis = StrictRedis(host=redisHost, port=6379, password=redisPassword, decode_responses=True)
    redis.set(f'checkers_state/portfolio/{ip}/domain', domain)
