import sys
from random import choice, randint
import re
import dns
from dns import resolver
import requests
import string
import html
from user_agent import generate_user_agent

STATUS_UP = 'UP'
STATUS_DOWN = 'DOWN'
STATUS_CORRUPT = 'CORRUPT'
STATUS_MUMBLE = 'MUMBLE'

timeout = 5
fake_exploits = [
    '\' OR 1=1 -- ',
    ' SELECT 1 ',
    ' UNION 1,2 ',
    ' ../../../../etc/passwd ',
    '; && cat /etc/passwd ',
    '; /bin/bash -c "echo test" #',
    '''<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE foo [
<!ELEMENT foo ANY >
<!ENTITY xxe SYSTEM "file:///etc/passwd" >]>
<foo>&xxe;</foo>'''
]

ua = generate_user_agent()


class CheckException(Exception):
    def __init__(self, status, description, error):
        self.status = status
        self.description = description
        self.error = error


class PortfolioNotFoundException(Exception):
    pass


def random_string(length: int):
    chars = string.ascii_letters + string.digits
    return ''.join(choice(chars) for _ in range(length))


def random_lowercase_string(length: int):
    chars = string.ascii_lowercase + string.digits
    return ''.join(choice(chars) for _ in range(length))


def validate_register(r: requests.Response):
    if r.status_code != 200:
        raise CheckException(STATUS_MUMBLE, f'/register returned code {r.status_code}', r.text)

    if '?user_exists' in r.request.path_url:
        raise CheckException(STATUS_MUMBLE, 'Registration failed (user exists)', r.text)

    if '?domain_exists' in r.request.path_url:
        raise CheckException(STATUS_MUMBLE, 'Registration failed (domain exists)', r.text)

    if len(r.cookies) == 0:
        raise CheckException(STATUS_MUMBLE, '/register did not set cookies', r.text)


def register_random(s: requests.Session, url: str):
    login = random_string(randint(5, 40))
    password = random_string(randint(10, 40))

    try:
        r = s.post(url + 'register',
                   timeout=timeout,
                   headers={
                       "User-Agent": ua
                   },
                   data={
                       "login": login,
                       "password": password,
                       "random_domain": "on",
                       "domain": ""
                   })
    except Exception as e:
        raise CheckException(STATUS_DOWN, 'Can\'t send register request', str(e))

    validate_register(r)

    result = r.text
    match = re.search(
        "<a class=\"nav-link js-scroll-trigger\" "
        "href=\"http:\\/\\/([a-z0-9]+)\\.portfolio\\.ctrl:3333\">Моё портфолио<\\/a>",
        result)

    if not match:
        raise CheckException(STATUS_MUMBLE, '/register result did not contain domain name', result)

    return login, password, match.group(1)


def register(s: requests.Session, url: str):
    login = random_string(randint(5, 40))
    password = random_string(randint(10, 40))
    domain = random_lowercase_string(randint(6, 32))

    try:
        r = s.post(url + 'register',
                   timeout=timeout,
                   headers={
                       "User-Agent": ua
                   },
                   data={
                       "login": login,
                       "password": password,
                       "domain": domain
                   })
    except Exception as e:
        raise CheckException(STATUS_DOWN, 'Can\'t send register request', str(e))

    validate_register(r)

    return login, password, domain


def edit_portfolio(s: requests.Session, url: str, content: str):
    try:
        r = s.post(url + 'edit',
                   timeout=timeout,
                   headers={
                       "User-Agent": ua
                   },
                   data={
                       "content": content
                   })
    except Exception as e:
        raise CheckException(STATUS_MUMBLE, 'Can\'t send edit portfolio request', str(e))

    if r.status_code != 200:
        raise CheckException(STATUS_MUMBLE, f'/edit returned code {r.status_code}', r.text)


def do_login(s: requests.Session, url: str, login: str, password: str):
    try:
        r = s.post(url + 'login',
                   timeout=timeout,
                   headers={
                       "User-Agent": ua
                   },
                   data={
                       "login": login,
                       "password": password
                   })
    except Exception as e:
        raise CheckException(STATUS_MUMBLE, 'Can\'t send login request', str(e))

    if r.status_code != 200:
        raise CheckException(STATUS_MUMBLE, f'/login returned code {r.status_code}', r.text)

    if '?incorrect' in r.request.path_url:
        raise CheckException(STATUS_MUMBLE, 'Login failed (incorrect credentials)', r.text)

    if len(r.cookies) == 0:
        raise CheckException(STATUS_MUMBLE, '/login did not set cookies', r.text)


def get_portfolio(s: requests.Session, url: str):
    try:
        r = s.get(url + 'edit',
                  timeout=timeout,
                  headers={
                      "User-Agent": ua
                  })
    except Exception as e:
        raise CheckException(STATUS_MUMBLE, 'Can\'t send edit portfolio request', str(e))

    if r.status_code != 200:
        raise CheckException(STATUS_MUMBLE, f'/edit returned code {r.status_code}', r.text)

    return html.unescape(r.text)


def open_portfolio(url: str, domain: str, error_status: str):
    try:
        r = requests.get(url,
                         timeout=timeout,
                         headers={
                             "User-Agent": ua,
                             "Host": domain + ".portfolio.ctrl:3333"
                         })
    except Exception as e:
        raise CheckException(STATUS_DOWN, 'Can\'t send open portfolio request', str(e))

    if r.status_code == 404:
        raise PortfolioNotFoundException

    if r.status_code != 200:
        raise CheckException(error_status, f'Open portfolio request returned code {r.status_code}', r.text)

    return html.unescape(r.text)


def get_latest_users(url: str):
    try:
        r = requests.get(url + 'latest',
                         timeout=timeout,
                         headers={
                             "User-Agent": ua
                         })
    except Exception as e:
        raise CheckException(STATUS_MUMBLE, 'Can\'t send latest users request', str(e))

    if r.status_code != 200:
        raise CheckException(STATUS_MUMBLE, f'/latest returned code {r.status_code}', r.text)

    return r.text


def resolve_domain(ip: str, domain: str, error_status: str):
    res = resolver.Resolver()
    res.nameservers = [ip]
    res.nameserver_ports = {ip: 3353}

    try:
        res.resolve(domain + '.portfolio.ctrl', raise_on_no_answer=True, lifetime=timeout)
    except dns.exception.Timeout as e:
        raise CheckException(error_status, 'DNS server did not answer in time', e.msg)
    except (dns.resolver.NXDOMAIN, dns.resolver.NoAnswer) as e:
        raise CheckException(error_status, 'DNS server failed to resolve domain', e.msg)


def generate_content():
    return random_string(randint(5, 15)) + choice(fake_exploits) + random_string(randint(5, 15))


def check_functionality(ip: str):
    try:
        url = 'http://' + ip + ':3333/'
        s = requests.Session()

        if randint(0, 1) == 0:
            login, password, domain = register(s, url)
        else:
            login, password, domain = register_random(s, url)

        content = generate_content()
        edit_portfolio(s, url, content)

        resolve_domain(ip, domain, STATUS_MUMBLE)

        latest_users = get_latest_users(url)
        if login not in latest_users:
            raise CheckException(STATUS_MUMBLE, 'GET /latest did not return registered user',
                                 'Returned users:\n' + latest_users)

        s2 = requests.Session()
        do_login(s2, url, login, password)
        got_content = get_portfolio(s2, url)

        if content not in got_content:
            raise CheckException(STATUS_MUMBLE, 'GET /edit did not return content', 'Returned content:\n' + got_content)

        try:
            opened_content = open_portfolio(url, domain.upper(), STATUS_MUMBLE)
        except PortfolioNotFoundException:
            raise CheckException(STATUS_MUMBLE, 'Portfolio was not found (uppercase domain)', domain.upper())

        if content not in opened_content:
            raise CheckException(STATUS_MUMBLE, 'Portfolio did not contain content',
                                 'Returned content:\n' + opened_content)

        print(STATUS_UP)
        print('Everything is fine')
    except CheckException as e:
        status = e.status
        description = e.description
        error = e.error

        exc_type, exc_obj, exc_tb = sys.exc_info()

        print(status)
        print(description)
        print('[line ' + str(exc_tb.tb_lineno) + '] ' + error, file=sys.stderr)
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()

        print('MUMBLE')
        print('Error during check')
        print('[line ' + str(exc_tb.tb_lineno) + '] ' + str(e), file=sys.stderr)


def push_flag(ip: str, flag: str):
    try:
        url = 'http://' + ip + ':3333/'
        s = requests.Session()
        _, _, domain = register_random(s, url)
        content = 'This is my flag: ' + flag
        edit_portfolio(s, url, content)

        print(STATUS_UP)
        print('Everything is fine')

        return domain
    except CheckException as e:
        status = e.status
        description = e.description
        error = e.error

        exc_type, exc_obj, exc_tb = sys.exc_info()

        print(status)
        print(description)
        print('[line ' + str(exc_tb.tb_lineno) + '] ' + error, file=sys.stderr)
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()

        print('MUMBLE')
        print('Error during push')
        print('[line ' + str(exc_tb.tb_lineno) + '] ' + str(e), file=sys.stderr)


def pull_flag(ip: str, domain: str, flag: str):
    try:
        url = 'http://' + ip + ':3333/'

        resolve_domain(ip, domain, STATUS_CORRUPT)

        try:
            content = open_portfolio(url, domain, STATUS_CORRUPT)
        except PortfolioNotFoundException:
            raise CheckException(STATUS_CORRUPT, 'Portfolio with flag was not found', domain)

        if flag not in content:
            raise CheckException(STATUS_CORRUPT, 'Portfolio did not contain flag', 'Returned content:\n' + content)

        print(STATUS_UP)
        print('Everything is fine')
    except CheckException as e:
        status = e.status
        description = e.description
        error = e.error

        exc_type, exc_obj, exc_tb = sys.exc_info()

        print(status)
        print(description)
        print('[line ' + str(exc_tb.tb_lineno) + '] ' + error, file=sys.stderr)
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()

        print('MUMBLE')
        print('Error during pull')
        print('[line ' + str(exc_tb.tb_lineno) + '] ' + str(e), file=sys.stderr)
