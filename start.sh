#!/usr/bin/env bash

if ! command -v ip &> /dev/null
then
  PORTFOLIO_EXTERNAL_IP=$(ifconfig | grep -Po 'inet (addr:)?\K[\d.]+' | grep '10\.80\.')
else
  PORTFOLIO_EXTERNAL_IP=$(ip -4 addr | grep -Po 'inet \K[\d.]+' | grep '10\.80\.')
fi

export PORTFOLIO_EXTERNAL_IP
docker-compose up --build -d