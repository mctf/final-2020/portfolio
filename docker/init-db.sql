CREATE DATABASE IF NOT EXISTS `pdns`;
CREATE DATABASE IF NOT EXISTS `portfolio`;

GRANT ALL PRIVILEGES ON pdns.* TO 'portfolio'@'%';
GRANT ALL PRIVILEGES ON portfolio.* TO 'portfolio'@'%';